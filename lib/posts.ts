import fs from 'fs'
import matter from 'gray-matter'
import path from 'path'
import { remark } from 'remark'
import html from 'remark-html'

const postsDirectory = path.join(process.cwd(), 'posts')

export function getSortedPostsData() {
  return getSortedPostsDirData([])
}

function getSortedPostsDirData(relativePaths: string[]) {
  // Get current directory path
  const parentPath = path.join(postsDirectory, ...relativePaths)
  // Get file names under /posts
  const dirents = fs.readdirSync(parentPath, { withFileTypes: true })
  let idByPostData: {
    date: string,
    title: string,
    id: string[],
    locale: string
  }[] = []
  dirents.forEach(dirent => {
    const direntName = dirent.name
    // Get file list if dirent is directory
    if (dirent.isDirectory()) {
      idByPostData = [...idByPostData, ...getSortedPostsDirData([...relativePaths, direntName])]
      return
    }

    // Remove ".md" from file name to get id
    // get locale
    const spliteDirentName = direntName.split(/\./)
    const id = spliteDirentName[0]
    const locale = spliteDirentName[1]
    if (spliteDirentName[2] !== "md") {
      return
    }
    // Read markdown file as string
    const fullPath = path.join(parentPath, direntName)
    const fileContents = fs.readFileSync(fullPath, 'utf8')

    // Use gray-matter to parse the post metadata section
    const matterResult = matter(fileContents)

    // Combine the data with the id
    idByPostData = [...idByPostData, {
      id: [...relativePaths, id],
      ...(matterResult.data as { date: string; title: string }),
      locale
    }]
  })
  // Sort posts by date
  return idByPostData.sort((a, b) => {
    if (a.date < b.date) {
      return 1
    } else {
      return -1
    }
  })
}

export function getAllPostIds() {
  return getSortedPostsDirData([]).map(postData => {
    return {
      params: {
        id: postData.id,
        locale: postData.locale
      },
      locale: postData.locale
    }
  })
}

export async function getPostData(id: string[], locale: string) {
  const fullPath = path.join(postsDirectory, `${path.join(...id)}.${locale}.md`)
  const fileContents = fs.readFileSync(fullPath, 'utf8')

  // Use gray-matter to parse the post metadata section
  const matterResult = matter(fileContents)

  // Use remark to convert markdown into HTML string
  const processedContent = await remark()
    .use(html)
    .process(matterResult.content)
  const contentHtml = processedContent.toString()

  // Combine the data with the id and contentHtml
  return {
    id,
    contentHtml,
    ...(matterResult.data as { date: string; title: string })
  }
}
