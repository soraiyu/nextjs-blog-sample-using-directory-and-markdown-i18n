[![Netlify Status](https://api.netlify.com/api/v1/badges/cfecaf04-1d30-4af4-a2d1-7afc317bbe6a/deploy-status)](https://app.netlify.com/sites/gentle-biscochitos-6cab20/deploys)

This is smaller blog sample using directory and Markdown and i18n.

URL is here: https://gentle-biscochitos-6cab20.netlify.app/

After clone this repository, you can run using follow command.
(node require)

1. `npm install`
1. `npm run dev`
1. you can see `localhost:3000`

How can you add your blog (i18n)
--
1. Set locale.

1. Save your file named [your-file.locale].md under posts/
1. You can see localhost:3000/posts/[your-file.locale]
1. Save your directory and file named [your-directory]/[your-file.locale].md under posts/
1. You can see  localhost:3000/posts/[your-directory]/[your-file.locale]

This site is made from <https://github.com/vercel/next-learn/tree/master/basics/dynamic-routes-starter>

And added this: Catch all routes: <https://nextjs.org/docs/routing/dynamic-routes#catch-all-routes>

i18n: <https://nextjs.org/docs/advanced-features/i18n-routing>