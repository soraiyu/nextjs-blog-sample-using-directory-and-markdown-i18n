import { GetStaticProps } from 'next'
import Head from 'next/head'
import Link from 'next/link'
import Date from '../components/date'
import Layout, { siteTitle } from '../components/layout'
import { getSortedPostsData } from '../lib/posts'
import utilStyles from '../styles/utils.module.css'

export default function Home({
  allPostsData,
  locale
}: {
  allPostsData: {
    date: string
    title: string
    id: string[]
    locale: string
  }[],
  locale: string
}) {
  return (
    <Layout home>
      <Head>
        <title>{siteTitle}</title>
      </Head>
      <section className={utilStyles.headingMd}>
        <p>
          This is sample page.<br />
          Source code is <a href="https://gitlab.com/soraiyu/nextjs-blog-sample-using-directory-and-markdown-i18n">https://gitlab.com/soraiyu/nextjs-blog-sample-using-directory-and-markdown-i18n</a><br />
          You'll find little explain at README.md<br />
          You’ll be building a site like this in <a href="https://nextjs.org/learn"> Next.js tutorial</a>.<br />
        </p>
      </section>
      <section className={`${utilStyles.headingMd} ${utilStyles.padding1px}`}>
        <h2 className={utilStyles.headingLg}>Blog</h2>
        <ul className={utilStyles.list}>
          {allPostsData.filter(d => d.locale === locale).map(({ id, date, title }) => (
            <li className={utilStyles.listItem} key={id.join("")}>
              <Link href={`/posts/${id.join("/")}`}>
                <a>{[...id.slice(0, -1), title].join("/")}</a>
              </Link>
              <br />
              <small className={utilStyles.lightText}>
                <Date dateString={date} />
              </small>
            </li>
          ))}
        </ul>
      </section>
    </Layout>
  )
}

export const getStaticProps: GetStaticProps = async ({ locale }) => {
  const allPostsData = getSortedPostsData()
  return {
    props: {
      allPostsData,
      locale
    }
  }
}